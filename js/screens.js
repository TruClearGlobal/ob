var loc = {
   data: [
      {name: 'Reception'},
      {name: 'Front display'},
      {name: 'Entrance'},
      {name: 'Foyer'},
      {name: 'Day Street'},
      {name: 'Customer waiting area'},
      {name: 'Streetside'},
      {name: 'Sales/Reception'},
      {name: 'Storefront'},
   ]
};

function replaceHeaderByline() {
   if(selected.client.id != undefined) {
      var subheading = document.getElementById('sub-heading');
      subheading.innerHTML = selected.client.name + ', ' + selected.site.name + ', ' + selected.inst.name;
   }
}

function loadScreens() {
   var tbody = document.getElementById('sites-data');
   for(var i = 0; i < selected.inst.count; i++) {
      var shouldShowError = (Math.floor((Math.random() * 10) + 1) == 5);

      var row = tbody.insertRow(tbody.rows.length);
      if(shouldShowError) {
         row.setAttribute('class', 'alert alert-danger');
      }

      var idCell = row.insertCell(0);
      var cellData = document.createTextNode(i + 1);
      idCell.appendChild(cellData);

      var randomLoc = Math.floor((Math.random() * loc.data.length));
      var nameCell = row.insertCell(1);
      cellData = document.createTextNode(loc.data[randomLoc].name);
      nameCell.appendChild(cellData);

      var siteCell = row.insertCell(2);
      cellData = document.createTextNode(selected.site.name);
      siteCell.appendChild(cellData);

      var clientCell = row.insertCell(3);
      cellData = document.createTextNode(selected.client.name);
      clientCell.appendChild(cellData);

      var locCell = row.insertCell(4);
      cellData = document.createTextNode(selected.site.loc);
      locCell.appendChild(cellData);

      var screenCell = row.insertCell(5);
      var screenCount = randomNumber(3);
      row.setAttribute('onClick', "setInstall(" + i + ", '" + loc.data[randomLoc].name + "', " + screenCount + ");");

      var dataRef = document.createElement('a');
      dataRef.setAttribute('href', '#');
      dataRef.setAttribute('id', 's' + i);
      dataRef.setAttribute('data-ajax-load', 'screens.html');
      dataRef.setAttribute('data-ajax-target', '#content');
      dataRef.setAttribute('data-loading-text', "<i class=\'fa fa-refresh fa-spin mr-xs\'></i> Loading...");

      if(shouldShowError) {
         var dataSpan = document.createElement('span');
         dataSpan.setAttribute('class', 'label label-danger text-gray-dark');

         cellData = document.createTextNode((screenCount - 1) + '/' + screenCount);
         dataRef.appendChild(cellData);
         dataSpan.appendChild(dataRef);
         screenCell.appendChild(dataSpan);
      }
      else {
         cellData = document.createTextNode(screenCount + '/' + screenCount);
         dataRef.appendChild(cellData);
         screenCell.appendChild(dataRef);
      }

      var statCell = row.insertCell(6);
      if(shouldShowError) {
         var alertType = Math.floor((Math.random() * 2) + 1);
         if(alertType == 1) {
            cellData = document.createTextNode('Heartbeat alert received');
         }
         else {
            cellData = document.createTextNode('Hardware alert received');
         }
      }
      else {
         var lastHeardFrom = Math.floor((Math.random() * 30) + 1);
         cellData = document.createTextNode('Last actionable event ' + lastHeardFrom + ' days ago');
      }
      statCell.appendChild(cellData);
   }
}

function setInstall(installId, installName, screenCount) {
   selected.inst.id = installId;
   selected.inst.name = installName;
   selected.scrn.count = screenCount;

   document.getElementById('s' + installId).click();
}

replaceHeaderByline();
loadScreens();
