var site = {
   data: [
      {id: "1",  name: "Manhattan branch",     loc: "New York, NY"},
      {id: "2",  name: "Main Showroom",        loc: "West Chester, PA"},
      {id: "3",  name: "Key West Location",    loc:"Key West, PA"},
      {id: "4",  name: "Dayton Branch",        loc: "Dayton, OH"},
      {id: "5",  name: "Louiseville",          loc: "Louiseville, KY"},
      {id: "6",  name: "Suburban Square",      loc: "Ardmore, PA"},
      {id: "7",  name: "Green Day Mall",       loc: "Lousieville, KY"},
      {id: "8",  name: "Mall of America",      loc: "Minneapolis, MN"},
      {id: "9",  name: "Alderwood",            loc: "Palo Alto, CA"},
      {id: "10", name: "Locust and Broad",     loc: "Philadelphia, PA"},
      {id: "11", name: "Peachtree Mall",       loc: "Columbus, GA"},
      {id: "12", name: "Granite Run Mall",     loc: "Media, PA"},
      {id: "13", name: "Reservoir Run",        loc: "Huntsville, AL"},
      {id: "14", name: "City Creek Center",    loc: "Salt Lake City, UT"},
      {id: "15", name: "Service Center",       loc: "Newport Beach, CA"},
      {id: "16", name: "Ranch San Antonio",    loc: "Chicaco, IL"},
      {id: "17", name: "Corto Medeira",        loc: "Lake Erie, PA"},
      {id: "18", name: "Milpitas Store",       loc: "Great Mall, Milpitas, CA"},
      {id: "19", name: "Arizona Mills",        loc: "Phoenix, AZ"},
      {id: "20", name: "Grand Canyon",         loc: "Flagstaff, AZ"},
      {id: "21", name: "Gilberte Gateway",     loc: "Mesa, AZ"},
      {id: "22", name: "Cedar Hills Crossing", loc: "Springfield, OR"},
      {id: "23", name: "Cascade Village",      loc: "Santa Monica, CA"},
      {id: "24", name: "Rogue Valley Mall",    loc: "South, OR"},
      {id: "25", name: "South Mall",           loc: "Birmingham, AL"},
      {id: "26", name: "Heritage Oaks",        loc: "Portland, OR"},
      {id: "27", name: "River Run Commons",    loc: "St Petersburg, FL"},
      {id: "28", name: "National Mall",        loc: "Washington, D.C."},
      {id: "29", name: "Lake Forest Center",   loc: "Seattle, WA"},
      {id: "30", name: "Mallard Mill Run",     loc: "Media, PA"},
   ]
};

function replaceHeaderByline() {
   if(selected.client.id != undefined) {
      var subheading = document.getElementById('sub-heading');
      subheading.innerHTML = selected.client.name;
   }
}

function loadSites() {
   var tbody = document.getElementById('sites-data');
   var siteLocation = '';
   for(var i = 0; i < 30; i++) {
      if(selected.client.name == site.data[i].client) {
         selected.site.loc = site.data[i].loc;
      }
   }

   for(var i = 0; i < selected.site.count; i++) {
      var randomSite = site.data[randomNumber(29)];
      var shouldShowError = (Math.floor((Math.random() * 10) + 1) == 5);
      var isNew = ((randomNumber(7) == 4) && ((i == 0) || (i == 1) || (i == 2))); // Some random magic to generate success probabistically...

      var row = tbody.insertRow(tbody.rows.length);

      if(!isNew && shouldShowError) {
         row.setAttribute('class', 'alert alert-danger');
      }

      var idCell = row.insertCell(0);
      var cellData = document.createTextNode(i + 1);
      idCell.appendChild(cellData);

      var nameCell = row.insertCell(1);
      cellData = document.createTextNode(randomSite.name + ' ');
      nameCell.appendChild(cellData);
      if(isNew) {
         var newLabelSpan = document.createElement('span');
         newLabelSpan.setAttribute('class', 'label label-success');
         var newBadge = document.createTextNode('New/Survey Requested');
         newLabelSpan.appendChild(newBadge);
         nameCell.appendChild(newLabelSpan);
      }

      var locCell = row.insertCell(2);
      cellData = document.createTextNode(randomSite.loc);
      locCell.appendChild(cellData);

      var clientCell = row.insertCell(3);
      cellData = document.createTextNode(selected.client.name);
      clientCell.appendChild(cellData);

      var instCell = row.insertCell(4);
      var instCount = randomNumber(3);
      row.setAttribute('onClick', "setSite(" + (i + 1) + ", " + instCount + ", '" + randomSite.name + "', '" + randomSite.loc + "');");

      var dataRef = document.createElement('a');
      dataRef.setAttribute('href', '#');
      dataRef.setAttribute('id', 's' + (i + 1));
      dataRef.setAttribute('data-ajax-load', 'installs.html');
      dataRef.setAttribute('data-ajax-target', '#content');
      dataRef.setAttribute('data-loading-text', "<i class=\'fa fa-refresh fa-spin mr-xs\'></i> Loading...");

      if((!isNew) && shouldShowError) {
         var dataSpan = document.createElement('span');
         dataSpan.setAttribute('class', 'label label-danger text-gray-dark');

         cellData = document.createTextNode((instCount - 1) + '/' + instCount);
         dataRef.appendChild(cellData);
         dataSpan.appendChild(dataRef);
         instCell.appendChild(dataSpan);
      }
      else {
         if(isNew) {
            cellData = document.createTextNode(instCount);
         }
         else {
            cellData = document.createTextNode(instCount + '/' + instCount);
         }
         dataRef.appendChild(cellData);
         instCell.appendChild(dataRef);
      }

      var statCell = row.insertCell(5);
      if(shouldShowError) {
         var alertType = Math.floor((Math.random() * 2) + 1);
         if(alertType == 1) {
            cellData = document.createTextNode('Heartbeat alert received');
         }
         else {
            cellData = document.createTextNode('Hardware alert received');
         }
      }
      else {
         var lastHeardFrom = Math.floor((Math.random() * 30) + 1);
         cellData = document.createTextNode('Last actionable event ' + lastHeardFrom + ' days ago');
      }
      statCell.appendChild(cellData);
   }
}

function setSite(siteId, instCount,  siteName, siteLocation) {
   selected.site.id = siteId;
   selected.site.name = siteName;
   selected.site.loc = siteLocation;
   selected.inst.count = instCount;

   // Display a random image to the user for the site schematic..
   var randomSchematic = randomNumber(2);
   var schematic = document.getElementById('schematic');
   schematic.setAttribute('src', 'img/installs/' + randomSchematic + 's.png');

   showMap();

   // Fill up info in the appropriate fields..
   var siteName = document.getElementById('siteName');
   siteName.value = selected.site.name;

   var city = document.getElementById('city');
   city.value = getCity(selected.site.loc);

   var state = document.getElementById('state');
   state.value = getState(selected.site.loc);
}

function getCity(fullLocation) {
   return fullLocation.substring(0, fullLocation.indexOf(","));
}

function getState(fullLocation) { 
   return fullLocation.substring(fullLocation.indexOf(',') + 2);
}

function addSite() {
   document.getElementById('s' + siteId).click();
}

function showSurveyDetails() {
}

function showInstalls() {
   document.getElementById('s' + selected.site.id).click();
}

function initGMap() {
   var uluru = {lat: 33.6610387, lng: -117.8554155};
   var map = new google.maps.Map(document.getElementById('tcgmap'), {
      zoom: 10,
      center: uluru
   });
   var marker = new google.maps.Marker({
      position: uluru,
      map: map
   });
}

replaceHeaderByline();
loadSites();
